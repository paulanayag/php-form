<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Aprendiendo PHP</title>
    <link href="https://fonts.googleapis.com/css?family=Proza+Libre" rel="stylesheet">
    <link rel="stylesheet" href="css/estilos.css" media="screen" title="no title">
  </head>
  <body>
    <div class="contenedor">
      <h1>Respuestas formulario</h1>
      <?php $resultado = $_POST; ?>
      <?php $nombre = $resultado['nombre']; ?>
      <?php $apellido = $resultado['apellido']; ?>

      <hr>
      <?php 
        if(!isset($nombre) || trim($nombre) != '') { ?>
          <p>Nombre: <?php echo $nombre; ?></p>
        <?php } else { 
          echo "El nombre es obligatorio</br>";
        }
      ?>

      <?php 
        if(!isset($apellido) || trim($apellido) != '') { ?>
          <p>Apellido: <?php echo $apellido; ?></p>
        <?php } else { 
          echo "El apellido es obligatorio</br>";
        }
      ?>

      <hr>

      <?php
        if(isset($_POST['notificaciones'])) {
          $notificaciones = $_POST['notificaciones'];
          if($notificaciones == 'on') {
            echo "Notificaciones activadas<hr>";
          }
        }
      ?>

      <?php
        if(isset($_POST['curso'])) {
          $array_cursos = $_POST['curso'];
          echo "Los cursos seleccionados son: </br>";
          foreach($array_cursos as $curso) {
            echo $curso . '</br>';
          }
        } else {
          echo "No tienes ningún curso seleccionado</br>";
        }
      ?>
      
      <hr>

      <?php
        if(isset($_POST['area'])) {
          $area = $_POST['area'];
          switch($area) {
            case 'fe':
              echo "El área de especialización es: </br>";
              echo "Front End</br>";
              break;
            case 'be':
              echo "El área de especialización es: </br>";
              echo "Back End</br>";
              break;
            case 'fs':
              echo "El área de especialización es: </br>";
              echo "Full Stack</br>";
              break;
            default:
             echo "No has seleccionado ningún área de especialización</br>";
              break;
          }
        }
      ?>

      <hr>

      <?php
        $opciones = array(
          'pres' => 'Presencial',
          'online' => 'Online'
        );
        if(array_key_exists('opciones', $_POST)) {
          $tipo_curso = $_POST['opciones'];
          switch($tipo_curso) {
            case 'pres':
              echo "Has elegido el curso presencial</br>";
              break;
            case 'online':
              echo "Has elegido el curso online</br>";
              break;
          }
        } else {
            echo "No has elegido el tipo de curso</br>";
        }
      ?>

      <hr>

      <?php
        if(isset($_POST['comentarios'])) {
          $comentarios = $_POST['comentarios'];
          $comentarios_aux = filter_var($comentarios, FILTER_SANITIZE_STRING);
          if(strlen($comentarios_aux) > 0 && trim($comentarios_aux)) {
            echo "Comentarios: </br>" . $comentarios_aux;
          }
        } else {
          echo "No hay comentarios";
        }
      ?>

      <hr>

    </div><!-- contenedor-->
  </body>
</html>
